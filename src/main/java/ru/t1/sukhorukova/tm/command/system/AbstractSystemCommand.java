package ru.t1.sukhorukova.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.service.ICommandService;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getLocatorService().getCommandService();
    }

    @Nullable @Override
    public Role[] getRoles() {
        return null;
    }

}
