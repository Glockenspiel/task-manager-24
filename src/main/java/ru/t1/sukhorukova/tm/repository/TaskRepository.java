package ru.t1.sukhorukova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @NotNull @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
